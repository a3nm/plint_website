# Website for plint

This repository contains the code of the Web frontend for
[plint](https://gitlab.com/a3nm/plint), that runs on
[plint.a3nm.net](https://plint.a3nm.net/).

To run the web interface on port 5000, run plint_web.py. Bottle and CherryPy are required
(but the code is easy to adapt), see
http://bottlepy.org/docs/dev/tutorial.html#deployment

Beware, if you use CherryPy under Debian, simply installing python-cherrypy3
might not work ("no module named cherrypy"). In this case, you need to install
CherryPy by hand from source (sudo python3 setup.py install).

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program (see file "COPYING").  If not, see <http://www.gnu.org/licenses/>.

Copyright (C) 2011-2019 by Antoine Amarilli
Repository URL: https://gitlab.com/a3nm/plint

